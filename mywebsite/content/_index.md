+++
title = "Home"
+++

# Welcome to My Personal Website

Hello! I'm Junhan Xu, a software developer passionate about creating innovative solutions to complex problems. This website showcases some of my projects and thoughts on technology, programming, and beyond. Feel free to explore and reach out if you're interested in collaborating or learning more.

## Projects

Below you'll find a list of my projects, each with a brief description and a link for more details.

### Rust AWS Lambda Function with API Gateway Integration

This project contains a Rust-based AWS Lambda function designed to process data and respond to HTTP requests, integrated with AWS API Gateway.

- [View Project](https://gitlab.oit.duke.edu/jx139/ids721-mini-project-2)

### AWS CDK S3 Bucket Creation with Versioning and Encryption

This repository contains code for creating an Amazon S3 bucket with versioning enabled and encryption using the AWS Cloud Development Kit (AWS CDK). AWS CodeWhisperer was utilized to help generate some of the CDK code for this project.

- [View Project](https://gitlab.oit.duke.edu/jx139/ids721-mini-project-3)

### Rust Actix Web Service

This project is a simple web service built with Rust and the Actix web framework. It demonstrates how to containerize a Rust Actix application using Docker for easy deployment and scaling.

- [View Project](https://gitlab.oit.duke.edu/jx139/ids721-mini-project-4#rust-actix-web-service)


Thank you for visiting my website!